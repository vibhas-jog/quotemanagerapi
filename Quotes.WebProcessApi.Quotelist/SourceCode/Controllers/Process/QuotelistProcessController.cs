﻿using Domain.QuoteManager.API.BusinessLogic.Entities;
using Domain.QuoteManager.API.DataIntegration.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Constants = Domain.QuoteManager.API.BusinessLogic.Entities.Constants;

namespace Domain.QuoteManagerProcess.API.Controllers.Process
{
    [Route("api/[controller]/V{ver:apiVersion}")]
    [ApiController]
    public class QuoteListController : BaseController
    {
        private IQuoteListLogic _quoteListLogic;


        public QuoteListController(IQuoteListLogic quoteListLogic, ILogger<QuoteListController> logger)
        {
            _logger = logger;
            _quoteListLogic = quoteListLogic;
        }


        [HttpPost]
        [Route("GetQuoteList")]
        public async Task<IActionResult> GetQuoteList([FromBody] Executequoteandsearchrequest quoteRequest)
        {
            this.LogMethodStart();
            this.LogInputParameters(JsonConvert.SerializeObject(quoteRequest));
            IActionResult retVal = null;
            Stopwatch stWatch = Stopwatch.StartNew();
            RetrieveQuoteAndSearchResponse serviceResponseOutput = null;
            try
            {


                //Work on model state check later
                if (!ModelState.IsValid || quoteRequest == null)
                {
                    //Getting Modelstate errors
                    var errorList = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();

                    var response = new RetrieveQuoteAndSearchResponse()
                    {
                        responsepreamble = new Responsepreamble()
                        {
                            responsestatus = Constants.Status.Failed,
                            statuscode = Constants.StatusCode.FourHundred,
                            responsemessage = ModelState.Values.ToString(),
                        }
                    };
                    return BadRequest(response);
                }

                serviceResponseOutput = await _quoteListLogic.GetQuoteList(quoteRequest);
                if (serviceResponseOutput.responsepreamble != null && serviceResponseOutput.responsepreamble.statuscode == Constants.ResponseMessages.ResponseCode_Passed)
                {
                    this.LogSuccess(JsonConvert.SerializeObject(serviceResponseOutput));
                    retVal = Ok(serviceResponseOutput);
                }
                else
                {
                    this.LogError(JsonConvert.SerializeObject(serviceResponseOutput));
                    retVal = StatusCode(Convert.ToInt32(serviceResponseOutput.responsepreamble.statuscode), serviceResponseOutput);
                }
            }
            catch (Exception ex)
            {

                this.LogError($"{quoteRequest.quotesearchrequest.requestpreamble?.ToString() }, Exception Message:{ex.Message}, StackTrace:{ex.StackTrace}");
                retVal = StatusCode(StatusCodes.Status500InternalServerError, new ApiErrorResponse(StatusCodes.Status500InternalServerError, ex.Message));

            }
            finally
            {
                stWatch.Stop();
                this.LogMethodEnd($"CountryCode:{quoteRequest.quotesearchrequest.requestpreamble?.isocountrycode}, Customer#:{quoteRequest.quotesearchrequest.requestpreamble.customernumber}, contact:{quoteRequest.quotesearchrequest.requestpreamble.customercontact}, ExecutionTime:{stWatch.ElapsedMilliseconds}");
            }


            return retVal;

        }

    }
}
