﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;

namespace Domain.QuoteManagerProcess.API.Controllers.Process
{
    public abstract class BaseController : Controller
    {
        protected ILogger _logger;

        protected void LogInfo(string message = null,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            this._logger.LogInformation("{0}", memberName);
            if (!String.IsNullOrEmpty(message))
            {
                this._logger.LogDebug("{0}", message);
                Console.WriteLine("DEBUG: {0}", message);
            }
        }

        protected void LogMethodStart(string message = null,
           [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            this._logger.LogInformation("{0} method started ", memberName);
            Console.WriteLine("LOG: {0} method started ", memberName);
            if (!String.IsNullOrEmpty(message))
            {
                this._logger.LogDebug(message);
                Console.WriteLine("DEBUG: {0}", message);
            }
        }

        protected void LogMethodEnd(string message = null,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            this._logger.LogInformation("{0} method ended", memberName);
            if (!String.IsNullOrEmpty(message))
            {
                this._logger.LogDebug(message);
                Console.WriteLine("DEBUG: {0}", message);
            }
        }

        protected void LogSuccess(string message,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            this._logger.LogInformation("{0} method returned {1} with success", memberName, message);
            Console.WriteLine("LOG: {0} method returned {1} with success", memberName, message);
        }
        protected void LogError(string message,
           [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            this._logger.LogError("{0} method returned {1} with error", memberName, message);
            Console.WriteLine("ERROR: {0} method returned {1} with error", memberName, message);
        }

        protected void LogInputParameters(string message,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            this._logger.LogInformation("{0} input parameters passed : {1} ", memberName, message);
            Console.WriteLine("LOG: {0} input parameters passed : {1} ", memberName, message);
        }
        protected void LogCorrelationInfo(string correlationid, string countryCode, string bcn,
            [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            this._logger.LogInformation("LOG: {0} CorrelationId : {1} CountryCode : {2} CustomerNumber : {3} ", memberName, correlationid, countryCode, bcn);
            Console.WriteLine("LOG: {0} CorrelationId : {1} CountryCode : {2} CustomerNumber : {3} ", memberName, correlationid, countryCode, bcn);
        }
    }
}
