﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Sdk.Logging;
using Domain.QuoteManager.API.BusinessLogic.Entities;
using Constants = Domain.QuoteManager.API.BusinessLogic.Entities.Constants;
using Microsoft.AspNetCore.Http;
using Domain.QuoteManager.API.DataIntegration.Interface;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Domain.QuoteManager.API.Controllers.Experience
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuoteListExperienceController : ControllerBase
    {
        private readonly ILogger _logger;
        private IQuoteListLogic _quoteListLogic;
        public QuoteListExperienceController(ILogger logger, IQuoteListLogic quoteListLogic)
        {
            _logger = logger;
            _quoteListLogic = quoteListLogic;
        }
        // GET: api/<QuoteListExperienceController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost()]
        [Route("GetQuoteList")]
        public async Task<IActionResult> GetQuoteList([FromBody] Executequoteandsearchrequest quoteRequest)
        {


            //Test newobject = new Test();
            //newobject.quotename = "TestQuote";
            //newobject.quotenumber = "123456";
            //newobject.quoteurl = "http://www.test.com";
            //newobject.status = "Active";
            //return newobject;
            //this.LogMethodStart();
            //this.LogInputParameters(JsonConvert.SerializeObject(quoteRequest));
            IActionResult retVal = null;
            RetrieveQuoteAndSearchResponse serviceResponseOutput = null;
            try
            {


                //Work on model state check later
                if (!ModelState.IsValid || quoteRequest == null)
                {
                    var response = new RetrieveQuoteAndSearchResponse()
                    {
                        responsepreamble = new Responsepreamble()
                        {
                            responsestatus = Constants.Status.Failed,
                            statuscode = Constants.StatusCode.FourHundred,
                            responsemessage = ModelState.Values.ToString(),
                        }
                    };
                    return BadRequest(response);
                }

                serviceResponseOutput = await _quoteListLogic.GetQuoteList(quoteRequest);
                if (serviceResponseOutput.responsepreamble != null && serviceResponseOutput.responsepreamble.statuscode == Constants.ResponseMessages.ResponseCode_Passed)
                {
                    //this.LogSuccess(JsonConvert.SerializeObject(serviceResponseOutput));
                    retVal = Ok(serviceResponseOutput);
                }
                else
                {
                    //this.LogSuccess(JsonConvert.SerializeObject(serviceResponseOutput));
                    retVal = Ok(serviceResponseOutput);
                }
            }
            catch (Exception ex)
            {

                //this.LogError($"{quoteRequest.quotesearchrequest.requestpreamble?.ToString() }, Exception Message:{ex.Message}, StackTrace:{ex.StackTrace}");
                retVal = StatusCode(StatusCodes.Status500InternalServerError, new ApiErrorResponse(StatusCodes.Status500InternalServerError, ex.Message));

            }


            return retVal;
        }


        // GET: api/<QuoteListController>
        //[HttpGet()]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}




        [HttpGet]
        [Route("GetQuotes")]
        public async Task<QuoteListResponse> Get(string customernumber, string isocountrycode, int pageIndex, int recordperPage, string Sorting, string SortingColumnName)
        {
            RetrieveQuoteAndSearchResponse serviceResponseOutput = null;
            IActionResult retVal = null;
            string[] RenewalSources = { "2000000", "1000000", "100000002" };
            Requestpreamble requestpreamble = new Requestpreamble()
            {
                customernumber = customernumber,
                isocountrycode = isocountrycode
            };
            RetrieveQuoteSearchRequest search = new RetrieveQuoteSearchRequest()
            {
                PageIndex = pageIndex,
                RecordPerPage = recordperPage,
                Sorting = Sorting,
                SortingColumnName = SortingColumnName

            };

            QuoteAndSearchRequest quotesearch = new QuoteAndSearchRequest()
            {
                requestpreamble = requestpreamble,
                retrievequoterequest = search
            };


            Executequoteandsearchrequest extquoteReq = new Executequoteandsearchrequest()
            {
                quotesearchrequest = quotesearch

            };
            serviceResponseOutput = await _quoteListLogic.GetQuoteList(extquoteReq);

            List<QuoteList> theQ = new List<QuoteList>();
            foreach (var quote in serviceResponseOutput.quotelist)
            {
                QuoteList q = new QuoteList();

                q.QuoteGuid = quote.QuoteGuid;
                q.QuoteName = quote.QuoteName;
                q.QuoteNumber = quote.QuoteNumber;

                if (quote.Source == "1000004")
                {
                    q.CreatedBy = quote.Contact;
                }
                else if (RenewalSources.Contains(quote.Source) && quote.quotetype == "Renewal")
                {
                    q.CreatedBy = "Renewal";
                }
                else
                {
                    q.CreatedBy = "Ingram Micro";
                }


                q.CreatedOn = quote.Created;
                q.EndUserName = quote.EndUserName;
                q.Modified = quote.Modified;
                q.Status = quote.Status == "Active" ? "Ready to Order" : quote.Status;
                q.QuotePrice = quote.TotalAmount;
                q.quoteexpirydate = quote.EffectiveTo;
                q.RevisionNumber = quote.RevisionNumber;
                q.QuoteNumber_rev = (quote.RevisionNumber != "0") ? quote.QuoteNumber + "_" + quote.RevisionNumber : quote.QuoteNumber;
                theQ.Add(q);
            }

            QuoteListResponse resp = new QuoteListResponse()
            {
                Quotes = theQ,
                TotalCount = serviceResponseOutput.TotalCount

            };



            //retVal = Ok(resp);
            return resp;

        }


        // POST api/<QuoteListExperienceController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<QuoteListExperienceController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<QuoteListExperienceController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
