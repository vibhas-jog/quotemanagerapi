﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class QuoteAppSettings
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Authorization { get; set; }
        public string ServiceURL { get; set; }
        public string QuoteSaveVersion_QM_19_10_0 { get; set; }
        public string QuoteCloseTemplateId { get; set; }
        public string QuoteActivateTemplateId { get; set; }
        public string QuoteEmailExceptionTemplateId { get; set; }
        public string USPartnerGoPortalUrl { get; set; }
        public string CAPartnerGoPortalUrl { get; set; }
        public int PNABatchCount { get; set; }
        public int ProductSearchBatchCount { get; set; }


        public int Timeout { get; set; }
        public string IntegrationUsersCount { get; set; }
        public Dictionary<string, Dictionary<string, string>> IntegrationUsersList { get; set; }

        public string ReplaceSpecialCharacters { get; set; }

        public string GetQuoteServiceURLs { get; set; }

    }
}
