﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class RetrieveQuoteAndSearchResponse
    {
        public Responsepreamble responsepreamble { get; set; }
        public int TotalCount { get; set; }

        public List<RetrieveQuoteSearchResponse> quotelist { get; set; }
    }

    public class QuoteList
    {
        public string QuoteGuid { get; set; }
        public string QuoteName { get; set; }
        public string QuoteNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? quoteexpirydate { get; set; }
        public string EndUserName { get; set; }
        public string Status { get; set; }
        public DateTime Modified { get; set; }
        public string RevisionNumber { get; set; }
        public string QuotePrice { get; set; }
        public string QuoteNumber_rev { get; set; }

    }

    public class QuoteListResponse
    {
        public int TotalCount { get; set; }
        public List<QuoteList> Quotes { get; set; }
    }
}
