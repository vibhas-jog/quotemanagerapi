using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Common.Sdk.Logging;
using Domain.QuoteManager.API.BusinessLogic.Entities;
using Domain.QuoteManager.API.DataIntegration.Interface;
using Domain.QuoteManager.API.BusinessLogic.Services;
using Domain.QuoteManager.API.BusinessLogic;

namespace Domain.QuoteManagerProcess.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            var builder = new ConfigurationBuilder()
             .AddJsonFile("./Main/appsettings.json", optional: true, reloadOnChange: true)
              .AddJsonFile($"./Main/appsettings.{hostingEnvironment.EnvironmentName}.json");

            Configuration = builder.Build();
        }

       

        // This method gets called by the runtime. Use this method to add services to the container.
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddControllers(c =>
            {
                c.Conventions.Add(new ControllerNameOverride());
            });

            
            services.AddApiVersioning(
               options =>
               {
                   options.ReportApiVersions = true;
                   options.AssumeDefaultVersionWhenUnspecified = true;
                   options.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                   options.ApiVersionSelector = new Microsoft.AspNetCore.Mvc.Versioning.CurrentImplementationApiVersionSelector(options);
               });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Domain.QuoteManagerProcess.API", Version = "v1" });
            });

            services.Add(new ServiceDescriptor(typeof(ILogger), new SplunkLogger()));
            services.Configure<QuoteAppSettings>(Configuration.GetSection("QuoteAppSettings"));
            //services.Configure<QuoteAppSettings>(Configuration.GetSection("QuoteAppSettings"));
            //services.Configure<LogFile>(Configuration.GetSection("LogFile"));

            //services.AddTransient<ICommonQuoteService, CommonQuoteService>();
            services.AddTransient<IQuoteListService, QuoteListService>();
            services.AddTransient<IQuoteListLogic, QuoteListLogic>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Domain.QuoteManagerProcess.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
