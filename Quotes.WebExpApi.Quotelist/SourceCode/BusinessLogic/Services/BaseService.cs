﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Domain.QuoteManager.API.BusinessLogic.Services
{
    public class BaseService
    {
        protected ILogger _logger;

        protected void LogError(string message)
        {
            this._logger.LogError(message);
            Console.WriteLine(message);
        }

        protected void LogInformation(string message)
        {
            this._logger.LogInformation(message);
            Console.WriteLine(message);
        }
    }
}
