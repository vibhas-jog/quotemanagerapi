﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Constants = Domain.QuoteManager.API.BusinessLogic.Entities.Constants;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web;
using Microsoft.Extensions.Logging;
using Domain.QuoteManager.API.DataIntegration.Interface;
using Domain.QuoteManager.API.BusinessLogic.Entities;
using Domain.QuoteManager.API.BusinessLogic.Helper;
//using Domain.QuoteManager.API.DataIntegration.DTOs;

namespace Domain.QuoteManager.API.BusinessLogic.Services
{
    public class QuoteListService : IQuoteListService
    {
        QuoteAppSettings _appSettings;
        QuoteAppSettings_APAC _appSettingsAPAc;
        QuoteAppSettings _appSetNA;
        private readonly ILogger _logger;
        //private readonly ICommonQuoteService _commonService;
        public QuoteListService(IOptions<QuoteAppSettings> appSettings, ILogger<QuoteListService> logger)
        {

            _appSettings = appSettings.Value;
            _logger = logger;

        }
       
        public async Task<RetrieveQuoteAndSearchResponse> GetQuoteList(Executequoteandsearchrequest quoteRequest)
        {
            string accountId = string.Empty;
            string contactId = string.Empty;
            RetrieveQuoteAndSearchResponse retrievequoteandsearchresponse = new RetrieveQuoteAndSearchResponse();
            try
            {



                //return await GetQuotes(quoteRequest); //using IM360 service

                // return await GetQuotes_v2(quoteRequest);

                return await GetQuotes(quoteRequest);

            }
            catch (Exception ex)
            {

                throw;
            }

        }

        private async Task<RetrieveQuoteAndSearchResponse> GetQuotes(Executequoteandsearchrequest quoteRequest)
        {
            RetrieveQuoteAndSearchResponse retrievequoteandsearchresponse = new RetrieveQuoteAndSearchResponse();
            #region Variables

            Account account = new Account();
            Contact contact = new Contact();
            // SBO sbo = new SBO();
            #endregion
            string correlationId = quoteRequest?.quotesearchrequest?.requestpreamble?.correlationid ?? "";

            string jsonString;
            jsonString = JsonConvert.SerializeObject(quoteRequest);


            string jsonResponse = await CRMAPIHelper.PostJsonDataWithReturn(
                _appSettings.GetQuoteServiceURLs, jsonString);


            var jsonRespObj = JObject.Parse(jsonResponse);

            bool isError = false;
            if (jsonRespObj.Value<JToken>("error") != null && jsonRespObj.Value<JToken>("error").HasValues)
            {
                isError = true;
                retrievequoteandsearchresponse = new RetrieveQuoteAndSearchResponse()
                {
                    responsepreamble = new Responsepreamble()
                    {
                        responsestatus = Constants.Status.Failed,
                        statuscode = Constants.StatusCode.FourHundred,
                        responsemessage = "Error while reteriving quote !!!" + jsonRespObj.Value<JToken>("error").Value<string>("message")
                    },
                    quotelist = new List<RetrieveQuoteSearchResponse>()
                };
                return retrievequoteandsearchresponse;
            }

            if (!jsonRespObj["quotelist"].HasValues)
            {
                #region Quote Not Found Message
                retrievequoteandsearchresponse = new RetrieveQuoteAndSearchResponse()
                {
                    responsepreamble = new Responsepreamble()
                    {
                        responsestatus = Constants.Status.Passed,
                        statuscode = Constants.StatusCode.TwoHundred,
                        responsemessage = Constants.StatusMessage.RecordsNotFound
                    },
                    quotelist = new List<RetrieveQuoteSearchResponse>()
                };

                #endregion
            }
            else if (!isError)
            {

                retrievequoteandsearchresponse = new RetrieveQuoteAndSearchResponse()
                {
                    responsepreamble = new Responsepreamble()
                    {
                        responsestatus = Constants.Status.Passed,
                        statuscode = Constants.StatusCode.TwoHundred,
                        responsemessage = Constants.StatusMessage.RecordsFound
                    },
                    quotelist = new List<RetrieveQuoteSearchResponse>()
                };

                foreach (var quotes in jsonRespObj["quotelist"])
                {
                    RetrieveQuoteSearchResponse retrievequoteresponse = new RetrieveQuoteSearchResponse();


                    retrievequoteresponse.QuoteGuid = quotes.Value<string>("quoteGuid");
                    retrievequoteresponse.QuoteName = quotes.Value<string>("quoteName");
                    retrievequoteresponse.QuoteNumber = quotes.Value<string>("quoteNumber");
                    retrievequoteresponse.QuoteUrl = quotes.Value<string>("quoteUrl");
                    retrievequoteresponse.DartNumber = quotes.Value<string>("dartNumber");
                    retrievequoteresponse.EndUserName = quotes.Value<string>("endUserName");
                    retrievequoteresponse.enduseraddress = quotes.Value<string>("enduseraddress");
                    retrievequoteresponse.quotetype = quotes.Value<string>("quotetype");
                    retrievequoteresponse.enduseraddress2 = quotes.Value<string>("enduseraddress2");
                    retrievequoteresponse.enduseraddress3 = quotes.Value<string>("enduseraddress3");
                    retrievequoteresponse.enduserid = quotes.Value<string>("enduserid");
                    retrievequoteresponse.DealId = quotes.Value<string>("dealId");
                    if (retrievequoteresponse.DealId != retrievequoteresponse.DartNumber)
                    {
                        retrievequoteresponse.DealId = retrievequoteresponse.DartNumber;
                    }
                    retrievequoteresponse.enduserphone = quotes.Value<string>("enduserphone");
                    retrievequoteresponse.enduserponumber = quotes.Value<string>("enduserponumber");
                    retrievequoteresponse.LanguageCode = quotes.Value<string>("languageCode");



                    retrievequoteresponse.endusercity = quotes.Value<string>("endusercity");
                    retrievequoteresponse.enduserstate = quotes.Value<string>("enduserstate");
                    retrievequoteresponse.enduserzipcode = quotes.Value<string>("enduserzipcode");
                    retrievequoteresponse.EstimateId = quotes.Value<string>("estimateId");
                    retrievequoteresponse.bidid = quotes.Value<string>("biddisplaynum");
                    retrievequoteresponse.TotalAmount = quotes.Value<string>("totalAmount");
                    retrievequoteresponse.Created = quotes.Value<DateTime>("createdon");
                    retrievequoteresponse.Modified = quotes.Value<DateTime>("modifiedon");
                    retrievequoteresponse.RevisionNumber = quotes.Value<string>("revisionNumber");
                    retrievequoteresponse.EffectiveFrom = quotes.Value<DateTime?>("effectiveFrom");
                    retrievequoteresponse.EffectiveTo = quotes.Value<DateTime?>("effectiveTo");
                    Principal createdBy = new Principal();
                    createdBy.Id = quotes["createdBy"].Value<string>("id");
                    createdBy.Name = quotes["createdBy"].Value<string>("name");
                    retrievequoteresponse.CreatedBy = createdBy;
                    retrievequoteresponse.Created = quotes.Value<DateTime>("created");

                    Principal modifiedBy = new Principal();
                    modifiedBy.Id = quotes["modifiedBy"].Value<string>("id");
                    modifiedBy.Name = quotes["modifiedBy"].Value<string>("name");
                    retrievequoteresponse.ModifiedBy = modifiedBy;
                    retrievequoteresponse.Modified = quotes.Value<DateTime>("modified");

                    retrievequoteresponse.Account = quotes.Value<string>("account");
                    retrievequoteresponse.AccountGuid = quotes.Value<string>("accountGuid");
                    retrievequoteresponse.AccountName = quotes.Value<string>("accountName");

                    account.Id = quotes["accountInfo"].Value<string>("id");
                    account.Name = quotes["accountInfo"].Value<string>("name");
                    account.BCN = quotes["accountInfo"].Value<string>("bcn");
                    retrievequoteresponse.AccountInfo = account;

                    retrievequoteresponse.Contact = quotes.Value<string>("contact");
                    retrievequoteresponse.ContactGuid = quotes.Value<string>("contactGuid");
                    retrievequoteresponse.ContactEmail = quotes.Value<string>("contactEmail");

                    contact.Id = quotes["contactInfo"].Value<string>("id");
                    contact.Name = quotes["contactInfo"].Value<string>("name");
                    contact.Email = quotes["contactInfo"].Value<string>("email");
                    retrievequoteresponse.ContactInfo = contact;

                    retrievequoteresponse.Source = quotes.Value<string>("source");

                    //#region SBO

                    //sbo.BidPricing = quotes["specialPricing"].Value<bool>("bidPricing");
                    //sbo.BidVersion = quotes["specialPricing"].Value<string>("bidVersion");
                    //sbo.SpecialPricingNo = quotes["specialPricing"].Value<string>("specialPricingNo");
                    //sbo.BidDisplayNo = quotes["specialPricing"].Value<string>("bidDisplayNo");
                    //if (quotes["specialPricing"].Value<DateTime>("priceDeviationStartDate") != null)
                    //{
                    //    sbo.PriceDeviationStartDate = quotes["specialPricing"].Value<DateTime>("priceDeviationStartDate");
                    //}
                    //if (quotes["specialPricing"].Value<DateTime>("priceDeviationExpiryDate") != null)
                    //{
                    //    sbo.PriceDeviationExpiryDate = quotes["specialPricing"].Value<DateTime>("priceDeviationExpiryDate");
                    //}
                    //sbo.MultiplePOValidity = quotes["specialPricing"].Value<bool>("multiplePOValidity");

                    //retrievequoteresponse.SpecialPricing = sbo;

                    //#endregion


                    retrievequoteresponse.Status = quotes.Value<string>("status");
                    retrievequoteresponse.StatusCode = quotes.Value<int>("statusCode");
                    retrievequoteresponse.StatusReason = quotes.Value<string>("statusReason");
                    retrievequoteresponse.StatusReasonCode = quotes.Value<int>("statusReasonCode");
                    retrievequoteandsearchresponse.quotelist.Add(retrievequoteresponse);
                }
                retrievequoteandsearchresponse.TotalCount = jsonRespObj.Value<int>("totalCount");

            }
            return retrievequoteandsearchresponse;
        }

        
    }
}
