﻿
using Domain.QuoteManager.API.BusinessLogic.Entities;
using Domain.QuoteManager.API.DataIntegration.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic
{
    public class QuoteListLogic : IQuoteListLogic
    {
        private IQuoteListService _quoteListService;

        public QuoteListLogic(IQuoteListService quoteListService)
        {
            _quoteListService = quoteListService;

        }

        public async Task<RetrieveQuoteAndSearchResponse> GetQuoteList(Executequoteandsearchrequest quoteRequest)
        {
            return await _quoteListService.GetQuoteList(quoteRequest);

        }
    }
}
