﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class RetrieveQuoteSearchResponse
    {
        /// <summary>
        /// Primary Key - Guid of Quote in string format.
        /// </summary>
        public string QuoteGuid { get; set; }

        /// <summary>
        /// Quote Name in string format.
        /// </summary>
        public string QuoteName { get; set; }
        /// <summary>
        /// Multiconfiguration count for RenewVue
        /// </summary>
        public int MultiConfigurationCount { get; set; }

        /// <summary>
        /// Quote Number in string format.
        /// </summary>
        public string QuoteNumber { get; set; }

        /// <summary>
        /// QuoteUrl in string format.
        /// </summary>
        public string QuoteUrl { get; set; }

        /// <summary>
        /// effectiveto in datetime format.
        /// </summary>
        public DateTime quoteexpirydate { get; set; }

        /// <summary>
        /// Quote Revision Number in string format.
        /// </summary>
        public string RevisionNumber { get; set; }

        public Account AccountInfo { get; set; }
        /// <summary>
        /// Account(BCN) in string format.
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// Contact guid in string format.
        /// </summary>
        public string AccountGuid { get; set; }

        /// <summary>
        /// Account Name in string format.
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Account Phone in string format.
        /// </summary>
        public string AccountPhone { get; set; }

        public Contact ContactInfo { get; set; }
        /// <summary>
        /// Contact in string format.
        /// </summary>
        public string Contact { get; set; }

        /// <summary>
        /// Contact guid in string format.
        /// </summary>
        public string ContactGuid { get; set; }

        /// <summary>
        /// Contact email in string format.
        /// </summary>
        public string ContactEmail { get; set; }

        public Currency CurrencyInfo { get; set; }

        /// <summary>
        /// Currency Name in string format.
        /// </summary>
        public string CurrencyName { get; set; }
        /// <summary>
        /// Currency Code in string format.
        /// </summary>
        public string CurrencyCode { get; set; }
        /// <summary>
        /// Currency Symbol in string format.
        /// </summary>
        public string CurrencySymbol { get; set; }
        /// <summary>
        /// EndUser Name in string format.
        /// </summary>
        public string EndUserName { get; set; }

        /// <summary>
        /// Price Deviation in string format.
        /// </summary>
        public string PriceDeviation { get; set; }

        /// <summary>
        /// Price Deviation Start Date in Date format.
        /// </summary>
        public DateTime? PriceDeviationStartDate { get; set; }

        /// <summary>
        /// Price Deviation Expiry Date in Date format.
        /// </summary>
        public DateTime? PriceDeviationExpiryDate { get; set; }

        /// <summary>
        /// Effective From in Date format.
        /// </summary>
        public DateTime? EffectiveFrom { get; set; }

        /// <summary>
        /// Effective To in Date format.
        /// </summary>
        public DateTime? EffectiveTo { get; set; }

        /// <summary>
        /// Customer Need in string format.
        /// </summary>
        public string CustomerNeed { get; set; }

        /// <summary>
        /// Solution Proposed in string format.
        /// </summary>
        public string SolutionProposed { get; set; }

        /// <summary>
        /// Internal Comments in string format.
        /// </summary>
        public string InternalComments { get; set; }

        /// <summary>
        /// Carrier Code in string format.
        /// </summary>
        public string CarrierCode { get; set; }
        /// <summary>
        /// Payment Terms in string format.
        /// </summary>
        public string PaymentTerms { get; set; }
        /// <summary>
        /// ShipToGSTIN in string format.
        /// </summary>
        public string ShipToGSTIN { get; set; }
        /// <summary>
        /// BillToGSTIN in string format.
        /// </summary>
        public string BillToGSTIN { get; set; }
        /// <summary>
        /// ShipFromGSTIN in string format.
        /// </summary>
        public string ShipFromGSTIN { get; set; }

        /// <summary>
        /// DisclaimerTerms1 in string format.
        /// </summary>
        public string DisclaimerTerms1 { get; set; }
        /// <summary>
        /// DisclaimerTerms2 in string format.
        /// </summary>
        public string DisclaimerTerms2 { get; set; }
        /// <summary>
        /// IMWarehouse in string format.
        /// </summary>
        public string IMWarehouse { get; set; }

        /// <summary>
        /// Vendor QuoteNumber in string format. 
        /// </summary>
        public string VendorQuoteNumber { get; set; }

        /// <summary>
        /// Source
        /// </summary>
        public string Source { get; set; }


        /// <summary>
        /// Language Code
        /// </summary>
        public string LanguageCode { get; set; }

        /// <summary>
        /// ccwquoteid
        /// </summary>
        public string ccwquoteid { get; set; }

        /// <summary>
        /// quotetype
        /// </summary>
        public string quotetype { get; set; }

        /// <summary>
        /// quotedisplay
        /// </summary>
        public string quotetypedisplay { get; set; }

        /// <summary>
        /// segmentpricingtype
        /// </summary>
        public string segmentpricingtype { get; set; }

        /// <summary>
        /// Request Status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        ///  Status Code.
        /// </summary>
        public int StatusCode { get; set; }
        /// <summary>
        /// Request Success/Failure Reason.
        /// </summary>
        public string StatusReason { get; set; }

        /// <summary>
        /// Request Success/Failure Reason Code.
        /// </summary>
        public int StatusReasonCode { get; set; }

        /// <summary>
        /// Country of Quote
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Created in datetime format.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// CreatedBy in Principal format.
        /// </summary>
        public Principal CreatedBy { get; set; }

        /// <summary>
        /// Modified in datetime format.
        /// </summary>
        public DateTime Modified { get; set; }

        /// <summary>
        /// ModifiedBy in Principal format.
        /// </summary>
        public Principal ModifiedBy { get; set; }

        /// <summary>
        /// Owner in Principal format.
        /// </summary>
        public Principal Owner { get; set; }

        /// <summary>
        /// Closed  datetime format.
        /// </summary>
        public DateTime? ClosedDate { get; set; }

        /// <summary>
        /// Dart Number in string format.
        /// </summary>
        public string DartNumber { get; set; }

        /// <summary>
        /// Total Amount
        /// </summary>
        public string TotalAmount { get; set; }

        /// <summary>
        /// Estimate Number
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// Estimate Number
        /// </summary>
        public string DealId { get; set; }

        /// <summary>
        /// Price Deviation Expiry Date
        /// </summary>
        public DateTime? DARTExpirationDate { get; set; }

        /// <summary>
        /// BCN Number
        /// </summary>
        public string BCN { get; set; }

        /// <summary>
        /// QuoteResellerPO
        /// </summary>
        public string QuoteResellerPO { get; set; }

        /// <summary>
        /// QuoteEnduserPO
        /// </summary>
        public string QuoteEnduserPO { get; set; }

        /// <summary>
        /// QuoteEndUserName
        /// </summary>
        public string QuoteEndUserName { get; set; }

        /// <summary>
        /// QuoteEndUserAcopId
        /// </summary>
        public string QuoteEndUserAcopId { get; set; }

        /// <summary>
        /// QuoteEndUserId
        /// </summary>
        public string QuoteEndUserId { get; set; }

        /// <summary>
        /// QuoteEndUserMST
        /// </summary>
        public string QuoteEndUserMST { get; set; }

        /// <summary>
        /// QuoteISR
        /// </summary>
        public string QuoteISR { get; set; }

        /// <summary>
        /// QuoteIsDefaultAddress
        /// </summary>
        public bool QuoteIsDefaultAddress { get; set; }

        /// <summary>
        /// QuoteSBOBidVersion
        /// </summary>
        public string QuoteSBOBidVersion { get; set; }

        /// <summary>
        /// QuoteBidVendorPricing
        /// </summary>
        public int QuoteBidVendorPricing { get; set; }

        /// <summary>
        /// QuotePriceDeviation
        /// </summary>
        public string QuotePriceDeviation { get; set; }

        /// <summary>
        /// QuotePriceDevStartDate
        /// </summary>
        public DateTime? QuotePriceDevStartDate { get; set; }

        /// <summary>
        /// QuotePriceDevStartDate
        /// </summary>
        public DateTime? QuotePriceDevExpiryDate { get; set; }

        /// <summary>
        /// QuoteEUAddress
        /// </summary>
        public string QuoteEUAddress { get; set; }

        /// <summary>
        /// QuoteEUAddress
        /// </summary>
        public string QuoteEUAddress2 { get; set; }

        /// <summary>
        /// QuoteEUAddress
        /// </summary>
        public string QuoteEUAddress3 { get; set; }

        /// <summary>
        /// EndUserAcopId in string format.
        /// </summary>
        public string enduseracopid { get; set; }

        /// <summary>
        /// EndUserId in string format.
        /// </summary>
        public string enduserid { get; set; }

        /// <summary>
        /// End user PO
        /// </summary>
        public string enduserponumber { get; set; }

        /// <summary>
        /// End User Contact ID(10000001)
        /// </summary>
        public string endusercontactid { get; set; }

        /// <summary>
        /// End User Address
        /// </summary>
        public string enduseraddress { get; set; }
        /// <summary>
        ///  End User Address2
        /// </summary>
        public string enduseraddress2 { get; set; }
        /// <summary>
        ///  End User Address3
        /// </summary>
        public string enduseraddress3 { get; set; }
        /// <summary>
        ///  End User City
        /// </summary>
        public string endusercity { get; set; }
        /// <summary>
        ///  End User State
        /// </summary>

        public string enduserstate { get; set; }
        /// <summary>
        ///  End User Country Code
        /// </summary>
        public string endusercountrycode { get; set; }
        /// <summary>
        /// End User Email
        /// </summary>
        public string enduseremail { get; set; }
        /// <summary>
        /// End User Phone
        /// </summary>
        public string enduserphone { get; set; }
        /// <summary>
        /// End User Zip Code
        /// </summary>
        public string enduserzipcode { get; set; }
        /// <summary>
        /// EndUser Contact Name in string format.
        /// </summary>
        public string endusercontactname { get; set; }

        public string bidid { get; set; }

        /// <summary>
        /// QuoteEUCity
        /// </summary>
        public string QuoteEUCity { get; set; }

        /// <summary>
        /// QuoteEUContact
        /// </summary>
        public string QuoteEUContact { get; set; }

        /// <summary>
        /// QuoteEUEmail
        /// </summary>
        public string QuoteEUEmail { get; set; }

        /// <summary>
        /// QuoteEUPhone
        /// </summary>
        public string QuoteEUPhone { get; set; }

        /// <summary>
        /// QuoteEUState
        /// </summary>
        public string QuoteEUState { get; set; }

        /// <summary>
        /// QuoteEUZip
        /// </summary>
        public string QuoteEUZip { get; set; }

        /// <summary>
        /// QuoteEUCountryCode
        /// </summary>
        public string QuoteEUCountryCode { get; set; }

        /// <summary>
        /// QuoteIsResEU
        /// </summary>
        public bool QuoteIsResEU { get; set; }

        /// <summary>
        /// QuoteResEUSeqNo
        /// </summary>
        public string QuoteResEUSeqNo { get; set; }

        /// <summary>
        /// QuoteResContactId
        /// </summary>
        public string QuoteResContactId { get; set; }

        /// <summary>
        /// QuoteEstimateId
        /// </summary>
        public string QuoteEstimateId { get; set; }
        public string QuoteEUEntitlementNumber { get; set; }
        public string VendorGuid { get; set; }
        public string VendorName { get; set; }
        public string VendorNumber { get; set; }
        public string VendorMasterNumber { get; set; }
        public string VendorSettingMessage { get; set; }
        public string VendorCountry { get; set; }
        public bool VendorShowDiscount { get; set; } = false;

        #region Bill To Fields
        /// <summary>
        /// Quotebillto_line1
        /// </summary>
        public string Quotebillto_line1 { get; set; }

        /// <summary>
        /// Quotebillto_line2
        /// </summary>
        public string Quotebillto_line2 { get; set; }

        /// <summary>
        /// Quotebillto_line3
        /// </summary>
        public string Quotebillto_line3 { get; set; }

        /// <summary>
        /// Quotebillto_city
        /// </summary>
        public string Quotebillto_city { get; set; }

        /// <summary>
        /// Quotebillto_stateorprovince
        /// </summary>
        public string Quotebillto_stateorprovince { get; set; }

        /// <summary>
        /// Quotebillto_postalcode
        /// </summary>
        public string Quotebillto_postalcode { get; set; }

        /// <summary>
        /// Quotebillto_country
        /// </summary>
        public string Quotebillto_country { get; set; }

        /// <summary>
        /// Quotebillto_composite
        /// </summary>
        public string Quotebillto_composite { get; set; }
        #endregion

        #region Ship To Fields
        /// <summary>
        /// Quoteshipto_line1
        /// </summary>
        public string Quoteshipto_line1 { get; set; }

        /// <summary>
        /// Quoteshipto_line2
        /// </summary>
        public string Quoteshipto_line2 { get; set; }

        /// <summary>
        /// Quoteshipto_line3
        /// </summary>
        public string Quoteshipto_line3 { get; set; }

        /// <summary>
        /// Quoteshipto_city
        /// </summary>
        public string Quoteshipto_city { get; set; }

        /// <summary>
        /// Quoteshipto_stateorprovince
        /// </summary>
        public string Quoteshipto_stateorprovince { get; set; }

        /// <summary>
        /// Quoteshipto_postalcode
        /// </summary>
        public string Quoteshipto_postalcode { get; set; }

        /// <summary>
        /// Quoteshipto_country
        /// </summary>
        public string Quoteshipto_country { get; set; }

        /// <summary>
        /// Quoteshipto_composite
        /// </summary>
        public string Quoteshipto_composite { get; set; }
        #endregion

        /// <summary>
        /// QuoteCopyBillToAddress
        /// </summary>
        public bool QuoteCopyBillToAddress { get; set; }

        /// <summary>
        /// QuoteIsEUAddressValid
        /// </summary>
        public bool QuoteIsEUAddressValid { get; set; }

        /// <summary>
        /// QuoteKeyprimaryVendor
        /// </summary>
        public string QuoteKeyprimaryVendor { get; set; }

        /// <summary>
        /// QuoteBidDisplayNum
        /// </summary>
        public string QuoteBidDisplayNum { get; set; }

        /// <summary>
        /// QuoteDealID
        /// </summary>
        public string QuoteDealID { get; set; }

        /// <summary>
        /// QuoteOpportunity
        /// </summary>
        public string QuoteOpportunity { get; set; }
        public string SubTotalAmount_AP { get; set; }
        public string SubTotalAmount_SP { get; set; }

        public string QuoteEndUserMarketSegmentFormatted { get; set; }
        public string QuoteEndUserMarketSegment { get; set; }

        public string ResellerOpeningMessage { get; set; }

        public string ResellerClosingMessage { get; set; }

        public string EndUserTotalAmount { get; set; }

        public decimal EndUserTotalProft { get; set; }

        public decimal EndUserShipping { get; set; }

        public decimal EndUserDiscount { get; set; }

        public decimal EndUserTax { get; set; }

        public string EndUserProfitValue { get; set; }

        // public string LanguageCode { get; set; }

        public string ResellerEmail { get; set; }
        /// <summary>
        /// Contact phone in string format.
        /// </summary>
        public string ResellerPhone { get; set; }

        /// Contact phone in string format.
        /// </summary>
        public string ResellerAddress1_line1 { get; set; }

        public string ResellerAddress1_line2 { get; set; }

        public string ResellerAddress1_city { get; set; }

        public string ResellerAddress1_country { get; set; }

        public string ResellerAddress1_postalcode { get; set; }

        public string ResellerAddress1_stateorprovince { get; set; }

        public string ResellerDiscount { get; set; }

        public string ResellerShipping { get; set; }

        public string ResellerTax { get; set; }

        public string EendUserSubTotal { get; set; }

        public string EntireQuoteMarkup { get; set; }

        public string EntireQuoteMarkupUnit { get; set; }

        public string EnduserCompanyName { get; set; }

        public string introPreamble { get; set; }

        public string purchaseInstructions { get; set; }

        public string disclaimer { get; set; }

        public string leasingCalculations { get; set; }

        public string leasingAmount { get; set; }

        public string leasingInstructions { get; set; }


        public int ConfigurationCount { get; set; }
        public decimal freightAmount { get; set; }
        public string OpportunityId { get; set; }
        public int ExpectedProductLineCount { get; set; }

        public decimal leaseRateValue { get; set; }
        public string salesdeskemail { get; set; }

        public string Ownerid { get; set; }
        public string PrimaryVendorCode { get; set; }
        public string CreatedByid { get; set; }
        public string MasterVendorNumber { get; set; }


    }
}
