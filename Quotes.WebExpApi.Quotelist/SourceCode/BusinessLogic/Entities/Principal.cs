﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class Principal
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
