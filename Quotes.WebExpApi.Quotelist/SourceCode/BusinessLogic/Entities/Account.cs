﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class Account
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string BCN { get; set; }
        public string Phone { get; set; }
        public string SalesBranchCode { get; set; }

    }
}
