﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class RetrieveQuoteSearchRequest
    {
        public string QuoteNumber { get; set; }
        public string Name { get; set; }
        public string DartId { get; set; }
        public string EndUserName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string source { get; set; }
        public string QuoteStatus { get; set; }
        public string validatecontact { get; set; }
        public string Sorting { get; set; }
        public string SortingColumnName { get; set; }
        public int PageIndex { get; set; } = 1;
        public int RecordPerPage { get; set; } = 10;
        public string RevisionNumber { get; set; }
        public string isRenewvueEnabled { get; set; }
        public string SearchRenewalQuotes { get; set; }
        public string isQuickQuotesEnabled { get; set; }
        public string CustomDateColumn { get; set; }
        public string DonotPrepopulateQuotes { get; set; }
    }
}
