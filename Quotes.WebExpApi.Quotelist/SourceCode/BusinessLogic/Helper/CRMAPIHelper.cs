﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Domain.QuoteManager.API.BusinessLogic.Entities;
using Domain.QuoteManager.API.BusinessLogic.Services;

namespace Domain.QuoteManager.API.BusinessLogic.Helper
{
    public class CRMAPIHelper
    {
        public async static Task<string> GetAccessToken(QuoteAppSettings quoteAppSetting)
        {
            var authContext = new AuthenticationContext(quoteAppSetting.Authorization);
            var clientCred = new ClientCredential(quoteAppSetting.ClientId, quoteAppSetting.ClientSecret);
            var authResult = await authContext.AcquireTokenAsync(quoteAppSetting.ServiceURL, clientCred);
            return authResult.AccessToken;
        }
        public async static Task<string> GetJson(string accessToken, string url)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            httpClient.DefaultRequestHeaders.Add("Prefer", "odata.include-annotations=OData.Community.Display.V1.FormattedValue");
            var httpResult = httpClient.GetAsync(url).Result;
            return await httpResult.Content.ReadAsStringAsync();
        }
        public async static Task<string> PostJsonDataWithReturn(string url, string jsonData)
        {

            HttpClient httpClient = new HttpClient();
            httpClient.Timeout = TimeSpan.FromMinutes(20);
            StringContent stringContent = new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage httpResponse = await httpClient.PostAsync(url, stringContent);
            return await httpResponse.Content.ReadAsStringAsync();

        }


    }
    public class CRMHelperDistributedLoadRequest : BaseService
    {
        public CRMHelperDistributedLoadRequest(ILogger logger)
        {
            _logger = logger;
        }
        public async Task<string> GetAccessTokenByDistributedLoadRequest(QuoteAppSettings quoteAppSetting)
        {

            try
            {
                int remainder = 0;
                int secounds = DateTime.Now.Second;

                remainder = secounds % int.Parse(quoteAppSetting.IntegrationUsersCount);
                var clientId = String.Empty;
                var clientSecret = String.Empty;

                switch (remainder)
                {
                    case 0:
                        clientId = quoteAppSetting.IntegrationUsersList["1"]["ClientId"];
                        clientSecret = quoteAppSetting.IntegrationUsersList["1"]["ClientSecret"];
                        this.LogInformation("Get access token for Integration User 1, log seconds " + secounds.ToString());
                        break;
                    case 1:
                        clientId = quoteAppSetting.IntegrationUsersList["2"]["ClientId"];
                        clientSecret = quoteAppSetting.IntegrationUsersList["2"]["ClientSecret"];
                        this.LogInformation("Get access token for Integration User 2, log seconds " + secounds.ToString());
                        break;
                    case 2:
                        clientId = quoteAppSetting.IntegrationUsersList["3"]["ClientId"];
                        clientSecret = quoteAppSetting.IntegrationUsersList["3"]["ClientSecret"];
                        this.LogInformation("Get access token for Integration User 3, log seconds " + secounds.ToString());
                        break;
                    case 3:
                        clientId = quoteAppSetting.IntegrationUsersList["4"]["ClientId"];
                        clientSecret = quoteAppSetting.IntegrationUsersList["4"]["ClientSecret"];
                        this.LogInformation("Get access token for Integration User 4, log seconds " + secounds.ToString());
                        break;
                    case 4:
                        clientId = quoteAppSetting.IntegrationUsersList["5"]["ClientId"];
                        clientSecret = quoteAppSetting.IntegrationUsersList["5"]["ClientSecret"];
                        this.LogInformation("Get access token for Integration User 5, log seconds " + secounds.ToString());
                        break;
                    default:
                        this.LogInformation("Get access token for default Integration User 1, log seconds " + secounds.ToString());
                        return await CRMAPIHelper.GetAccessToken(quoteAppSetting);

                }

                var authContext = new AuthenticationContext(quoteAppSetting.Authorization);
                var clientCred = new ClientCredential(clientId, clientSecret);
                var authResult = await authContext.AcquireTokenAsync(quoteAppSetting.ServiceURL, clientCred);
                return authResult.AccessToken;


            }
            catch (Exception ex)
            {

                this.LogError("GetAccessTokenByDistributedLoadRequest Exception " + ex.Message);
                return await CRMAPIHelper.GetAccessToken(quoteAppSetting);

            }



        }
    }
}
