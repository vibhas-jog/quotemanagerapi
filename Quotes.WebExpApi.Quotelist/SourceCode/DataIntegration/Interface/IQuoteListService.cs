﻿using Domain.QuoteManager.API.BusinessLogic.Entities;
//using Domain.QuoteManager.API.DataIntegration.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.DataIntegration.Interface
{
    public interface IQuoteListService
    {
        Task<RetrieveQuoteAndSearchResponse> GetQuoteList(Executequoteandsearchrequest quoteRequest);
    }
}
