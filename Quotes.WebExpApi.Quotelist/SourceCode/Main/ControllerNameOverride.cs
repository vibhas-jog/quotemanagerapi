﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

public class ControllerNameOverride : IControllerModelConvention
{
    void IControllerModelConvention.Apply(ControllerModel controller)
    {
        if (controller != null)
        {
            foreach (var attribute in controller.Attributes)
            {
                if (attribute.GetType() == typeof(RouteAttribute))
                {
                    var routeAttribute = (RouteAttribute)attribute;

                    if (!string.IsNullOrWhiteSpace(routeAttribute.Name))
                    {
                        //replace the controller name with the route attribute's name if present
                        controller.ControllerName = routeAttribute.Name;
                    }
                }
            }
        }
    }
}
